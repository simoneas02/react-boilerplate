# Title of the project

> Description of the project

## Run the project local


**0 -** install the basic dependencies

- [NodeJS](https://nodejs.org/en/)

**1 -** Clone the project and install the dependencies:

```sh
$ git clone https://github.com/user-name/project-name
$ cd project-name/
$ yarn
```

**2 -** Start development mode:

```sh
$ yarn start
```

Go to: [localhost:8080](http://localhost:8080)


## Automatic Tasks

- `$ yarn start`: Watch the files to build and start a static server.
- `$ yarn build`: Compile and minify public/bundle.js.


## Contributing

Find on our [issues](https://github.com/user-name/project-name/issues/) the next steps of the project ;)  
Want to contribute? [Follow these recommendations](https://github.com/user-name/project-name/blob/master/CONTRIBUTING.md).


## History

See [Releases](https://github.com/user-name/project-name/releases) for detailed changelog.


## License

[MIT License](https://github.com/user-name/project-name/blob/master/LICENSE.md) © [Your Name](https://user-name.github.io)
